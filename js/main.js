
    $(document).ready(function () {

        $('.miniature').click(function(){
            var idToLink = '../img/quantron/' + this.id +'.jpg';
            $('#product').attr('src', idToLink);
        });

        $('.product-image-carousel').vertLSCarousel(
            {
                items:2,
                animationTime:400,
                // navText:['<span>up</span>','down'],
                navText: ["<img src='./img/quantron/strzalka.svg'>", "<img src='./img/quantron/strzalka.svg'>"],
                slideBy:2
            }
        );

        $('.prod').on('click',function(e){
            e.preventDefault();
            var src = $(this).data('src');
            var mainImage = $('.product-image');
            mainImage.find('.dot').remove();
            mainImage.find('img').attr('src',src);
            var points = $(this).data('points');

            $.each(points, function(number,value){
                $('<div><span>+</span></div>').addClass('dot').css({top:value[0]+'px',left:value[1]+'px'}).appendTo(mainImage);
            });
        });

        $('.navbar-toggle').click(function(){
            $('.navbar-collapse').toggleClass('right');
            $('.navbar-toggle').toggleClass('indexcity');
        });

        var tl1 = new TimelineMax({repeat:-1,yoyo:true});
        tl1.to('.back.square-left,.back.square-right',3,{rotation:3});

        var winHalf = $(document).width()/2;
        $('#top-container').mousemove(function (e) {
            var moveX = -(e.pageX/20) + 50,
            left=true;
            if(e.pageX>winHalf){
                left = false;
                moveX = -(e.pageX - winHalf)/20;
            }
            $('.back.robot').css({"-webkit-transform":"translate(" + moveX + "px,0)"})
        });


        $('#top-container .icon-wrapper a').on('click tap',function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            $("html, body").animate({ scrollTop: $(id).offset().top - 100 }, 1000);
        });




        $('.appear').appear();

        $(document.body).on('appear', '.appear', function(event, $all_appeared_elements) {
            var elem =  $(this);
            elem.addClass('active');
        });

        $.force_appear();


        var quoteSlider = $('#quote-slider');

        // quoteSlider.on('initialize.owl.carousel initialized.owl.carousel',function(e){
        //     console.log(dots);
        // });

        quoteSlider.owlCarousel({
            items:1,
            animateOut: 'slideOutDown',
            animateIn: 'slideInDown',
            loop:true,
            nav:true,
            dots:true,
            navText:['<img src="./img/quantron/strzalka.svg">','<img src="./img/quantron/strzalka.svg">'],
            onInitialized:function(e){
                var dotsWidth  = $('#quote-slider').find('.owl-dots').height();
                $('#quote-slider').find('.owl-nav').width(dotsWidth + 60);
            }
        });

        $('#close-register').on('click',function (e) {
            e.preventDefault();
            $('#register').hide('fast');
        });
        $('#register-btn').on('click',function (e) {
            e.preventDefault();
            $('#register').show('fast');
        });


        $('.item').click(function(){
            var idToSRC = './img/quantron/'+ this.id +'';
            $('#product').attr('src', idToSRC);
        });

        $('.navbar-center a').on('click', function(){
            $('.navbar-center').find('.active').removeClass('active');
            $(this).parent().addClass('active');
        });

        $('.navbar-right a').on('click', function(){
            $('.navbar-right').find('.active').removeClass('active');
            $(this).parent().addClass('active');
        });

    });
