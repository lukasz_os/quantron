(function($){

    $.fn.vertLSCarousel = function(options){

        var options = $.extend({
            items:3,
            animationTime:400,
            navText:['prev','next'],
            slideBy:1
        },options);


        return this.each(function(){

            var carousel = $(this);
            var carouselItems = carousel.children();
            carousel.html('');
            var imagesCarousel = $('<div></div>').addClass('images-carousel').appendTo(carousel);
            var imagesInside = $('<div></div>').addClass('images-inside').css({top:0}).appendTo(imagesCarousel);
            carouselItems.appendTo(imagesInside);
            imagesInside.children().wrap('<div class="item" />');
            var prev = $('<a></a>').addClass('carousel-nav prev').attr('href',"#").html(options.navText[0]).appendTo(carousel);
            var next = $('<a></a>').addClass('carousel-nav next').attr('href',"#").html(options.navText[1]).appendTo(carousel);

            var imagesInsideHeight = imagesInside.height();
            var length = imagesInside.children().length;
            var itemHeight = imagesInside.find('.item').height();
            var visibleHeight = itemHeight*options.items;
            imagesCarousel.height(visibleHeight);
            var maxOffset = -(imagesInsideHeight - visibleHeight);
            var scroll = false;
            var slideOffset = itemHeight*options.slideBy;

            imagesInside.css({'transition':options.animationTime+"ms",'-webkit-transition':options.animationTime+"ms"});


            next.on('click tap',function(e){
                e.preventDefault();
                var top =  imagesInside.outerHeight(true) - imagesInsideHeight;
                console.log(top);
                if(!scroll && top-slideOffset >= maxOffset){
                    scroll = true;
                    setTimeout(function(){
                        scroll=false;
                    },options.animationTime);
                    imagesInside.css({'margin-top':top-slideOffset});
                }
            });


            prev.on('click tap',function(e){
                e.preventDefault();
                var top = imagesInside.outerHeight(true) - imagesInsideHeight;
                console.log(top);
                if(!scroll && top<=0-slideOffset){
                    scroll=true;
                    setTimeout(function(){
                        scroll=false;
                    },options.animationTime);
                    imagesInside.css({'margin-top':top+slideOffset});
                }
            });

        });//end each loop
    }//end function
})($);
